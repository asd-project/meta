import os

from conans import ConanFile, CMake

project_name = "meta"


class MetaConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "asd meta utilities"
    topics = ("asd", "meta", "preprocessor", "templates")
    generators = "cmake"
    exports_sources = "include*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.build_tools/0.0.1@asd/testing",
        "boost/[>=1.79]",
        "magic_enum/0.6.5"
    )

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.hpp", dst="include", src="include")

    def package_info(self):
        self.info.header_only()
