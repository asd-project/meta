//---------------------------------------------------------------------------

#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <launch/main.h>
#include <meta/tuple/sort.h>
#include <meta/tuple/enumerate.h>

#include <entt/core/type_info.hpp>

#include <algorithm/string.h>

//---------------------------------------------------------------------------

namespace asd::app
{
    using namespace std::string_view_literals;
    
    template <class T>
    struct type_projection
    {
        static constexpr auto value = entt::type_name<T>::value();
    };
    
    static constexpr std::array type_names = {"char"sv, "float"sv, "int"sv, "struct asd::meta::types<>"sv, "unsigned int"sv};

    TEST_CASE("meta::sort", "[asd::meta]") {
        constexpr auto sorted_types = meta::sort<type_projection>(meta::types_v<meta::empty_t, int, char, float, entt::id_type>);
        static_assert(sorted_types == meta::types_v<char, float, int, struct asd::meta::types<>, entt::id_type>);

        meta::enumerate(sorted_types, [](auto index, auto type) {
            static_assert(entt::type_name<plaintype(type)::type>::value() == std::get<plaintype(index)::value>(type_names));
        });
    }

    static ::asd::entrance open([](int argc, char * argv[]) {
        constexpr auto type = entt::type_name<int>::value();
        Catch::Session session;

        if (int return_code = session.applyCommandLine(argc, argv); return_code != 0) {
            return return_code;
        }

        return session.run();
    });
}

//---------------------------------------------------------------------------
