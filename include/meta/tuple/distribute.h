//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <size_t N, class T>
    constexpr auto distribute(T && value) {
        constexpr auto forward_with_index = [](auto && value, auto) -> T && {
            return std::forward<T>(value);
        };

        return apply_indices<N>([&]<size_t ... Index>(std::index_sequence<Index...>) {
            return std::forward_as_tuple(forward_with_index(value, std::integral_constant<size_t, Index>{})...);
        });
    }
}
