//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/apply.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    // Just like meta::apply but only for a single tuple.
    // The order of arguments is more consistent with other functions too.
    template <meta::tuple_like Tuple, class F>
    constexpr auto unpack(Tuple && t, F && f) {
        return meta::apply(std::forward<F>(f), std::forward<Tuple>(t));
    }
}
