//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/types/find_form_of.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <size_t ... Indices, class Tuple>
        constexpr auto make_swizzle_sequence(const Tuple & t, meta::types<>) {
            return std::index_sequence<Indices...>{};
        }

        template <size_t ... Indices, class Tuple, class Head, class ... A>
        constexpr auto make_swizzle_sequence(const Tuple & t, meta::types<Head, A...>) {
            constexpr int Index = meta::find_form_of_v<Tuple, Head>;
            static_assert(Index >= 0, "Couldn't find applicable type in provided tuple");

            return make_swizzle_sequence<Indices..., Index>(t, meta::types_v<A...>);
        }
    }

    template <class Tuple, class ... A>
    constexpr auto make_swizzle_sequence(const Tuple & t, meta::types<A...> types) {
        return asd::meta::detail::make_swizzle_sequence(t, types);
    }

    template <class ... A, class Tuple>
    constexpr auto swizzle(Tuple && t, meta::types<A...> types) {
        return select(std::forward<Tuple>(t), make_swizzle_sequence(t, types));
    }

    template <class ... A, class Tuple>
    constexpr auto swizzle(Tuple && t) {
        return swizzle(std::forward<Tuple>(t), meta::types_v<A...>);
    }
}
