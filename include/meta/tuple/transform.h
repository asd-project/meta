//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/apply.h>
#include <meta/types/nth.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class Tuple, class F>
        requires (std::tuple_size_v<plain<Tuple>> > 0 && meta::meta_type<std::invoke_result_t<F, first_t<Tuple>>>)
    constexpr auto transform(Tuple && t, F && f) {
        return meta::apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            return (... + std::forward<F>(f)(get<Index>(t)));
        }, t);
    }

    template <class Tuple, class F>
        requires (std::tuple_size_v<plain<Tuple>> > 0 && not meta::meta_type<std::invoke_result_t<F, first_t<Tuple>>>)
    constexpr auto transform(Tuple && t, F && f) {
        return meta::apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            return std::make_tuple(std::forward<F>(f)(get<Index>(std::forward<Tuple>(t)))...);
        }, t);
    }

    template <class Tuple, class F>
        requires (std::tuple_size_v<plain<Tuple>> == 0)
    constexpr auto transform(Tuple && t, F &&) {
        return std::forward<Tuple>(t);
    }
}
