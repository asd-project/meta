//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/types/nth.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class Tuple, class F>
    constexpr void for_each(Tuple && t, F && f) {
        apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            (..., f(get<Index>(std::forward<Tuple>(t))));
        }, std::forward<Tuple>(t));
    }

    template <class Tuple, class F>
        requires(std::tuple_size_v<plain<Tuple>> == 0)
    auto co_for_each(Tuple && t, F && f) -> std::invoke_result_t<F, meta::empty_t> {
        co_return;
    }

    template <class Tuple, class F>
        requires(std::tuple_size_v<plain<Tuple>> > 0)
    auto co_for_each(Tuple && t, F && f) -> std::invoke_result_t<F, first_t<Tuple> &&> {
        co_await co_apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) -> std::invoke_result_t<F, first_t<Tuple> &&> {
            (..., co_await f(get<Index>(std::forward<Tuple>(t))));
            co_return;
        }, std::forward<Tuple>(t));

        co_return;
    }
}
