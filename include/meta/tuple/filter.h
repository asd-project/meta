//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/tuple/select.h>
#include <meta/types/filter.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <template <class> class Predicate, class Tuple>
    constexpr auto filter(Tuple && t) {
        return select(std::forward<Tuple>(t), make_filter_sequence<Predicate>(t));
    }
}
