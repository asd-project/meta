//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/find.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class T, class Tuple>
    constexpr bool contains(Tuple && t, T value) {
        return meta::find(std::forward<Tuple>(t), value) >= 0;
    }
}
