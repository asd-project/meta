//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <size_t Index, class Tuple, class T>
        constexpr int find_value(Tuple && t, T value) {
            if constexpr (Index == std::tuple_size_v<plain<Tuple>>) {
                return -1;
            } else {
                auto v = get<Index>(std::forward<Tuple>(t));

                if constexpr (std::equality_comparable_with<plaintype(value), plaintype(v)>) {
                    if (v == value) {
                        return static_cast<int>(Index);
                    }
                }

                return find_value<Index + 1>(std::forward<Tuple>(t), value);
            }
        }
    }

    template <class Tuple, class T>
    constexpr int find(Tuple && t, T value) {
        return asd::meta::detail::find_value<0>(std::forward<Tuple>(t), value);
    }
}
