//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/apply.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class ... T, class ... Rest>
    constexpr auto concat(const std::tuple<T...> & tuple, Rest && ... rest) {
        return meta::apply([]<class ... A>(A && ... args) {
            return std::make_tuple(std::forward<A>(args)...);
        }, tuple, std::forward<Rest>(rest)...);
    }

    template <class ... T, class ... Rest>
    constexpr auto concat(std::tuple<T...> && tuple, Rest && ... rest) {
        return meta::apply([]<class ... A>(A && ... args) {
            return std::make_tuple(std::forward<A>(args)...);
        }, std::move(tuple), std::forward<Rest>(rest)...);
    }

    template <class ... T, class ... Rest>
    constexpr auto concat(meta::types<T...> types, Rest && ... rest) {
        return meta::apply([]<class ... A>(A && ... types) {
            return (... + types);
        }, types, std::forward<Rest>(rest)...);
    }
}
