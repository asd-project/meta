//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/types/concat.h>

//---------------------------------------------------------------------------

namespace asd::meta::tuple
{
    template <class ... T, size_t... Indices>
    constexpr auto flat_select(const std::tuple<T...> & tuple, std::index_sequence<Indices...>) {
        return std::tuple_cat(meta::as_tuple(get<Indices>(std::forward<Tuple>(tuple)))...);
    }

    template <class ... T, size_t... Indices>
    constexpr auto flat_select(std::tuple<T...> && tuple, std::index_sequence<Indices...>) {
        return std::tuple_cat(meta::as_tuple(get<Indices>(std::forward<Tuple>(tuple)))...);
    }

    template <class ... T, size_t... Indices>
    constexpr auto flat_select(meta::types<T...> types, std::index_sequence<Indices...>) {
        using Types = meta::types<T...>;
        return meta::concat_t<typename nth_t<Indices, Types>::type...>{};
    }
}
