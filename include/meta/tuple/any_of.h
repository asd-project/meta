//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class Values, class Tuple>
    constexpr bool any_of(Values && values, Tuple && t) {
        return apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            return (std::forward<F>(f)(get<Index>(std::forward<Tuple>(t))) || ...);
        }, values);
    }
}
