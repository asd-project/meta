//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/tuple/select.h>
#include <meta/types/filter.h>
#include <meta/types/remove_if.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <template <class> class Predicate, class Tuple>
    constexpr auto partition(Tuple && t) {
        return std::make_pair(
            select(std::forward<Tuple>(t), make_filter_sequence<Predicate>(t)),
            select(std::forward<Tuple>(t), make_remove_if_sequence<Predicate>(t))
        );
    }
}
