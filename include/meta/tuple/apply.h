//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    // std::apply explicitely uses std::get, we can use more general approach
    template <class F, meta::tuple_like Tuple, meta::tuple_like ... Rest>
    constexpr auto apply(F && f, Tuple && t, Rest && ... rest) {
        return apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            if constexpr (sizeof ... (Rest) > 0) {
                return meta::apply([&]<class ... A>(A && ... args) {
                    return std::forward<F>(f)(get<Index>(std::forward<Tuple>(t))..., std::forward<A>(args)...);
                }, std::forward<Rest>(rest)...);
            } else {
                return std::forward<F>(f)(get<Index>(std::forward<Tuple>(t))...);
            }
        }, t);
    }
}
