//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/contains.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class Tuple, class Values>
    constexpr bool contains_any_of(Tuple && t, Values && values) {
        return apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            return (meta::contains(std::forward<Tuple>(t), get<Index>(values)) || ...);
        }, values);
    }
}
