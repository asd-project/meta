//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class Tuple, class F>
    constexpr bool all_of(Tuple && t, F && f) {
        return apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            return (std::forward<F>(f)(get<Index>(std::forward<Tuple>(t))) && ...);
        }, values);
    }
}
