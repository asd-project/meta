//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class Tuple>
    constexpr auto unique(Tuple && t) {
        // TODO: Add actual implementation ._.
        return std::forward<Tuple>(t);
    }
}
