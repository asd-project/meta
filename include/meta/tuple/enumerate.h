//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class Tuple, class F>
    constexpr void enumerate(Tuple && t, F f) {
        apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
            (..., f(std::integral_constant<size_t, Index>{}, get<Index>(std::forward<Tuple>(t))));
        }, t);
    }
}
