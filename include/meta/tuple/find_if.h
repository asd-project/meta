//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <size_t Index, class Tuple, class F>
        constexpr int find_if(Tuple && t, F && predicate) {
            if constexpr (Index == std::tuple_size_v<plain<Tuple>>) {
                return -1;
            } else {
                auto && v = get<Index>(std::forward<Tuple>(t));
                auto && result = predicate(v);

                if constexpr (plaintype(result)::value) {
                    return static_cast<int>(Index);
                }

                return find_value<Index + 1>(std::forward<Tuple>(t), std::forward<F>(predicate));
            }
        }
    }

    template <class Tuple, class F>
    constexpr int find_if(Tuple && t, F && predicate) {
        return asd::meta::detail::find_if<0>(std::forward<Tuple>(t), std::forward<F>(predicate));
    }

    template <size_t Index, class Tuple, class F>
    constexpr int find_if(Tuple && t, F && predicate) {
        return asd::meta::detail::find_if<Index>(std::forward<Tuple>(t), std::forward<F>(predicate));
    }
}
