//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "accumulate.h"
#include "apply.h"
#include "enumerate.h"
#include "flat_select.h"
#include "for_each.h"
#include "partition.h"
#include "select.h"
#include "slice.h"
