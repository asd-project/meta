//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/select.h>
#include <numeric>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class T, size_t N, class Iterator, class F>
    constexpr auto sort_indices(const std::array<T, N> & values, Iterator left, Iterator right, F predicate) {
        if (left >= right) {
            return;
        }

        auto start = left;

        for (auto i = std::next(start); i < right; i++) {
            if (predicate(values[*i], values[*start])) {
                meta::swap(*++left, *i);
            }
        }

        meta::swap(*start, *left);

        sort_indices(values, start, left, predicate);
        sort_indices(values, std::next(left), right, predicate);
    }

    template <class T, size_t N, class F>
    constexpr auto sort_indices(const std::array<T, N> & values, F predicate) {
        std::array<size_t, N> indices;
        std::iota(indices.begin(), indices.end(), 0);

        meta::sort_indices(values, indices.begin(), indices.end(), predicate);

        return indices;
    }

    template <template<class> class Projection, class Predicate = std::less<>, class ... T> requires requires {
        { Projection<typename meta::first_t<meta::types<T...>>::type>::value };
    }
    constexpr auto sort(meta::types<T...> types) {
        if constexpr(sizeof...(T) <= 1) {
            return types;
        } else {
            return meta::apply_indices([&]<size_t ... Index>(std::index_sequence<Index...>) {
                constexpr auto Indices = sort_indices(std::array{Projection<T>::value...}, Predicate{});
                return meta::select(types, std::index_sequence<Indices[Index]...>{});
            }, types);
        }
    }
}
