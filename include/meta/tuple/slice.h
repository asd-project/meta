//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/tuple/select.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <size_t Begin, size_t End, class Tuple>
        requires(Begin <= End && End <= std::tuple_size_v<Tuple>)
    constexpr auto slice(Tuple && t) {
        return [&]<size_t ... Index>(std::index_sequence<Index...>) {
            return select(std::forward<Tuple>(t), std::index_sequence<(Begin + Index)...>{});
        }(std::make_index_sequence<End - Begin>{});
    }
}
