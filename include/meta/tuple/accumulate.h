//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <size_t Index, class Accumulator, class Tuple, class F>
        constexpr auto accumulate(Tuple && t, Accumulator && acc, F f) {
            if constexpr (Index == std::tuple_size_v<plain<Tuple>> - 1) {
                return f(std::forward<Accumulator>(acc), get<Index>(std::forward<Tuple>(t)));
            } else {
                return accumulate<Index + 1>(std::forward<Tuple>(t), f(std::forward<Accumulator>(acc), get<Index>(std::forward<Tuple>(t))), f);
            }
        }
    }

    template <class Init, class Tuple, class F>
    constexpr auto accumulate(Tuple && t, Init && init, F f) {
        if constexpr (std::tuple_size_v<plain<Tuple>> == 0) {
            return std::forward<Init>(init);
        } else {
            return asd::meta::detail::accumulate<0>(std::forward<Tuple>(t), std::forward<Init>(init), f);
        }
    }
}
