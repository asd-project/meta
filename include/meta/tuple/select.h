//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/types/nth.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class ... T, size_t... Indices>
    constexpr auto select(std::tuple<T...> & tuple, std::index_sequence<Indices...>) {
        return std::forward_as_tuple(get<Indices>(tuple)...);
    }

    template <class ... T, size_t... Indices>
    constexpr auto select(const std::tuple<T...> & tuple, std::index_sequence<Indices...>) {
        using Types = std::tuple<T...>;
        return std::tuple<nth_t<Indices, Types>...>(get<Indices>(tuple)...);
    }

    template <class ... T, size_t... Indices>
    constexpr auto select(std::tuple<T...> && tuple, std::index_sequence<Indices...>) {
        using Types = std::tuple<T...>;
        return std::tuple<nth_t<Indices, Types>...>(get<Indices>(std::move(tuple))...);
    }

    template <class ... T, size_t... Indices>
    constexpr auto select(meta::types<T...> types, std::index_sequence<Indices...>) {
        using Types = std::tuple<T...>;
        return meta::types_v<nth_t<Indices, Types>...>;
    }
}
