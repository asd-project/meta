//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "common.h"

#include <variant>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class ... T>
    struct types
    {
        using tuple_type = std::tuple<T...>;

        static constexpr size_t count = sizeof ... (T);
    };

    template <>
    struct types<>
    {
        using tuple_type = std::tuple<>;

        static constexpr size_t count = 0;
    };

    template <class T>
    struct types<T>
    {
        using type = T;
        using tuple_type = std::tuple<T>;

        static constexpr size_t count = 1;
    };

    template <class R, class ... Arguments>
    struct types<R(Arguments...)>
    {
        using type = R(Arguments...);
        using return_type = R;
        using arguments_type = std::tuple<Arguments...>;

        static constexpr size_t count = 1;
    };

    template <class ... T>
    types(T ... args) -> types<typename T::type...>;

    template <class T>
    using type = types<T>;

    template <class T>
    constexpr type<T> type_v;

    template <class ... T>
    constexpr types<T...> types_v;

    using empty_t = types<>;
    constexpr empty_t empty;

    constexpr bool is_empty(auto && v) {
        return std::is_same_v<plaintype(v), meta::empty_t>;
    }

    template <class ... A, class ... B>
    constexpr bool operator == (types<A...>, types<B...>) {
        return std::is_same_v<types<A...>, types<B...>>;
    }

    template <class ... A, class ... B>
    constexpr bool operator != (types<A...>, types<B...>) {
        return !std::is_same_v<types<A...>, types<B...>>;
    }

    template <class ... A, class ... B>
    constexpr auto operator + (types<A...>, types<B...>) {
        return meta::types_v<A..., B...>;
    }

    template <class A, class B>
    constexpr auto operator | (types<A>, types<B>) {
        return meta::types_v<std::variant<A, B>>;
    }

    template <class ... A, class B>
    constexpr auto operator | (types<std::variant<A...>>, types<B>) {
        return meta::types_v<std::variant<A..., B>>;
    }

    template <class A, class ... B>
    constexpr auto operator | (types<A>, types<std::variant<B...>>) {
        return meta::types_v<std::variant<A, B...>>;
    }

    template <class ... A, class ... B>
    constexpr auto operator | (types<std::variant<A...>>, types<std::variant<B...>>) {
        return meta::types_v<std::variant<A..., B...>>;
    }

    namespace detail
    {
        template <class T>
        struct is_meta_type : std::false_type {};

        template <class ... T>
        struct is_meta_type<types<T...>> : std::true_type {};

        template <class T>
        struct is_meta_type<const T> : is_meta_type<T> {};

        template <class T>
        struct is_meta_type<T &> : is_meta_type<T> {};
    }

    template <class T>
    using is_meta_type = meta::detail::is_meta_type<T>;

    template <class T>
    constexpr bool is_meta_type_v = meta::is_meta_type<T>::value;

    template <class T>
    concept meta_type = is_meta_type_v<T>;
}
