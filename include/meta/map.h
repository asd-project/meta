//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "common.h"

//---------------------------------------------------------------------------

namespace asd
{
    namespace meta
    {
        template <typename Element>
        class iterator
        {
        public:
            constexpr iterator(const Element * pos) noexcept :
                _pos(pos) {}

            constexpr bool operator == (const iterator & rhs) const noexcept {
                return _pos == rhs._pos;
            }

            constexpr bool operator != (const iterator & rhs) const noexcept {
                return _pos != rhs._pos;
            }

            constexpr iterator & operator++() noexcept {
                ++_pos;
                return *this;
            }

            constexpr iterator & operator+=(size_t i) noexcept {
                _pos += i;
                return *this;
            }

            constexpr iterator operator+(size_t i) const noexcept {
                return _pos + i;
            }

            constexpr iterator & operator--() noexcept {
                --_pos;
                return *this;
            }

            constexpr iterator & operator-=(size_t i) noexcept {
                _pos -= i;
                return *this;
            }

            constexpr size_t operator-(const iterator & rhs) const noexcept {
                return _pos - rhs._pos;
            }

            constexpr const auto & operator*() const noexcept {
                return *_pos;
            }

            constexpr const auto * operator-> () const noexcept {
                return &*_pos;
            }

        private:
            const Element * _pos;
        };

        template <typename Iterator, typename Key, typename F>
        constexpr auto map_lower_bound(Iterator left, Iterator right, const Key & key, F predicate) noexcept {
            size_t count = right - left;

            while (count > 0) {
                const size_t step = count / 2;
                right = left + step;

                if (predicate(right->first, key)) {
                    left = ++right;
                    count -= step + 1;
                } else {
                    count = step;
                }
            }

            return left;
        }

        template <typename Iterator, typename Key, typename F>
        constexpr auto map_upper_bound(Iterator left, Iterator right, const Key & key, F predicate) noexcept {
            size_t count = right - left;

            while (count > 0) {
                const size_t step = count / 2;
                right = left + step;

                if (!predicate(key, right->first)) {
                    left = ++right;
                    count -= step + 1;
                } else {
                    count = step;
                }
            }

            return left;
        }

        template <typename Iterator, typename F>
        constexpr void map_sort(Iterator left, Iterator right, F predicate) noexcept {
            if (left >= right) {
                return;
            }

            const Iterator start = left;

            for (auto i = std::next(start); i < right; i++) {
                if (predicate(i->first, start->first)) {
                    meta::swap(*(++left), *i);
                }
            }

            meta::swap(*start, *left);

            map_sort(start, left, predicate);
            map_sort(std::next(left), right, predicate);
        }

        template <typename Key, typename Value, size_t N, class Comparator = std::less<>>
        class map
        {
        public:
            using key_type = Key;
            using mapped_type = Value;
            using value_type = std::pair<Key, Value>;
            using size_type = size_t;
            using difference_type = std::ptrdiff_t;
            using const_reference = const value_type &;
            using const_pointer = const value_type *;
            using const_iterator = typename std::array<value_type, N>::const_iterator;

            static constexpr Comparator comparator{};

            constexpr map(const value_type (& data)[N]) noexcept : map(data, std::make_index_sequence<N>()) {}

            constexpr map(value_type (&& data)[N]) noexcept : map(std::move(data), std::make_index_sequence<N>()) {}

            constexpr map(const std::array<value_type, N> & a) noexcept : _data {a} {
                map_sort(_data.begin(), _data.end(), comparator);
            }

            constexpr map(std::array<value_type, N> && a) noexcept : _data {std::move(a)} {
                map_sort(_data.begin(), _data.end(), comparator);
            }

            constexpr map(map && m) noexcept = default;

            constexpr bool unique() const noexcept {
                for (auto it = _data.begin(), right = std::prev(_data.end()); it < right; ++it) {
                    if (!comparator(*it, *std::next(it))) {
                        return false;
                    }
                }

                return true;
            }

            constexpr const mapped_type & at(const key_type & key) const {
                auto it = find(key);

                if (it == end()) {
                    BOOST_THROW_EXCEPTION(std::out_of_range("key not found"));
                }

                return it->second;
            }

            constexpr const mapped_type & operator[](const key_type & key) const noexcept {
                return find(key)->second;
            }

            constexpr size_t size() const noexcept {
                return _data.size();
            }

            constexpr const_iterator begin() const noexcept {
                return _data.cbegin();
            }

            constexpr const_iterator cbegin() const noexcept {
                return _data.cbegin();
            }

            constexpr const_iterator end() const noexcept {
                return _data.cend();
            }

            constexpr const_iterator cend() const noexcept {
                return _data.cend();
            }

            constexpr const_iterator lower_bound(const key_type & key) const noexcept {
                return map_lower_bound(_data.cbegin(), _data.cend(), key, comparator);
            }

            constexpr const_iterator upper_bound(const key_type & key) const noexcept {
                return map_upper_bound(_data.cbegin(), _data.cend(), key, comparator);
            }

            constexpr std::pair<const_iterator, const_iterator> equal_range(const key_type & key) const noexcept {
                auto first = map_lower_bound(_data.cbegin(), _data.cend(), key, comparator);
                return {first, map_upper_bound(first, _data.cend(), key, comparator)};
            }

            constexpr size_t count(const key_type & key) const noexcept {
                const auto range = equal_range(key);
                return range.second - range.first;
            }

            constexpr const_iterator find(const key_type & key) const noexcept {
                auto it = lower_bound(key);
                return it != _data.cend() && !comparator(key, it->first) ? it : _data.cend();
            }

            constexpr bool contains(const key_type & key) const noexcept {
                auto it = lower_bound(key);
                return it != _data.cend() && !comparator(key, it->first);
            }

            const std::array<value_type, N> & base() const noexcept {
                return _data;
            }

        private:
            std::array<value_type, N> _data;

            template <size_t... I>
            constexpr map(const value_type (& data)[N], std::index_sequence<I...>) noexcept :
                map {std::array<value_type, N>{data[I]...}} {}

            template <size_t... I>
            constexpr map(value_type (&& data)[N], std::index_sequence<I...>) noexcept :
                map {std::array<value_type, N>{std::move(data[I])...}} {}
        };

        template <class K, class V, size_t N>
        map(std::pair<K, V> pairs[N]) -> map<K, V, N>;

        template <class K, class V, class Comparator, size_t N>
        map(std::pair<K, V> pairs[N]) -> map<K, V, N, Comparator>;

        template <class K, class V, size_t N>
        constexpr map<K, V, N> make_map(std::pair<K, V> (&& pairs)[N]) noexcept {
            return {std::move(pairs)};
        }

        template <class K, class V, class Comparator, size_t N>
        constexpr map<K, V, N, Comparator> make_map(std::pair<K, V> (&& pairs)[N]) noexcept {
            return {std::move(pairs)};
        }

        struct hash_comparator
        {
            template <class A, class B>
            constexpr bool operator ()(const A & a, const B & b) const noexcept {
                return std::hash<A>{}(a) < std::hash<B>{}(b);
            }
        };
    }
}
