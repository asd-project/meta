//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/types.h>
#include <meta/concepts.h>

#include <tuple>

//---------------------------------------------------------------------------

namespace std
{
    template <size_t Index, class ... T>
    struct tuple_element<Index, ::asd::meta::types<T...>> : std::tuple_element<Index, std::tuple<::asd::meta::type<T>...>> {};

    template <class ... T>
    struct tuple_size<::asd::meta::types<T...>> : std::integral_constant<size_t, sizeof...(T)> {};
}

namespace asd::meta
{
    template <size_t Index, class ... T>
    constexpr auto get(types<T...>) {
        return std::tuple_element_t<Index, meta::types<T...>>{};
    }

    template <class T>
    concept accessible_tuple = requires (T v) {
        { get<0>(v) };
    };

    template <class T>
    concept tuple_like = requires {
        { std::tuple_size<plain<T>>::value } -> std::convertible_to<size_t>;
        requires (std::tuple_size<plain<T>>::value == 0 || accessible_tuple<plain<T>>);
    };

    template <class>
    struct is_tuple : std::false_type {};

    template <class ... T>
    struct is_tuple<std::tuple<T...>> : std::true_type {};

    template <class V>
    constexpr decltype(auto) as_tuple(V && v) {
        if constexpr(meta::is_tuple<plain<V>>::value) {
            return std::forward<V>(v);
        } else {
            return std::forward_as_tuple(std::forward<V>(v));
        }
    }

    template <class F, class ... Tuples, class R = decltype(std::declval<F>()(std::make_index_sequence<std::tuple_size_v<Tuples>>{}...))>
        requires (!awaitable<R>)
    constexpr auto apply_indices(F f, const Tuples & ...) {
        return f(std::make_index_sequence<std::tuple_size_v<Tuples>>{}...);
    }

    template <class F, class ... Tuples, class R = decltype(std::declval<F>()(std::make_index_sequence<std::tuple_size_v<Tuples>>{}...))>
        requires awaitable<R>
    auto co_apply_indices(F f, const Tuples & ...) -> R {
        co_return co_await f(std::make_index_sequence<std::tuple_size_v<Tuples>>{}...);
    }

    template <size_t N, class F, class R = decltype(std::declval<F>()(std::make_index_sequence<N>{}))>
        requires (!awaitable<R>)
    constexpr auto apply_indices(F f) {
        return f(std::make_index_sequence<N>{});
    }

    template <size_t N, class F, class R = decltype(std::declval<F>()(std::make_index_sequence<N>{}))>
        requires awaitable<R>
    auto co_apply_indices(F f) -> R {
        co_return co_await f(std::make_index_sequence<N>{});
    }
}
