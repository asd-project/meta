//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/common.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class T>
    struct unique_value_traits
    {
        static constexpr T default_value() {
            return T{};
        }
    };

    template <class T, class Traits = unique_value_traits<T>>
    struct unique_value
    {
        unique_value() noexcept :
            value(Traits::default_value())
        {}

        unique_value(T value) noexcept :
            value(std::move(value))
        {}

        unique_value(unique_value && u) noexcept :
            value(std::exchange(u.value, Traits::default_value()))
        {}

        unique_value & operator = (unique_value && u) noexcept {
            value = std::exchange(u.value, Traits::default_value());
            return *this;
        }

        bool valid() const {
            return value != Traits::default_value();
        }

        operator bool () const {
            return valid();
        }

        operator T () const requires (std::is_pod_v<T>) {
            return value;
        }

        operator const T & () const requires (!std::is_pod_v<T>) {
            return value;
        }

        operator T & () requires (!std::is_pod_v<T>) {
            return value;
        }

        const auto ptr() const {
            if constexpr (std::is_pointer_v<T>) {
                return value;
            } else {
                return &value;
            }
        }

        auto ptr() {
            if constexpr (std::is_pointer_v<T>) {
                return value;
            } else {
                return &value;
            }
        }

        const auto operator & () const {
            return ptr();
        }

        auto operator & () {
            return ptr();
        }

        const auto operator -> () const {
            return ptr();
        }

        auto operator -> () {
            return ptr();
        }

        friend bool operator == (const unique_value &, const unique_value &) = default;
        friend std::strong_ordering operator <=> (const unique_value &, const unique_value &) = default;

        T value;
    };

    template <class T, class Traits, class U>
        requires(!std::same_as<unique_value<T, Traits>, plain<U>> && requires(const T & a, const U & b) {
            { a == b };
        })
    constexpr bool operator == (const unique_value<T, Traits> & a, const U & b) {
        return a.value == b;
    }

    template <class T, class Traits, class U>
        requires(!std::same_as<unique_value<T, Traits>, plain<U>> && requires(const T & a, const U & b) {
            { a <=> b };
        })
    constexpr std::strong_ordering operator <=>(const unique_value<T, Traits> & a, const U & b) {
        return a.value <=> b;
    }
}
