//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        struct subscript
        {
            template <class T1, class T2>
            constexpr decltype(auto) operator()(T1 && t1, T2 && t2) const {
                return std::forward<T1>(t1)[std::forward<T2>(t2)];
            }
        };
    }

    template <int I>
    struct placeholder
    {
        template <class... A>
        constexpr decltype(auto) operator()(A &&... a) const noexcept {
            return get<size_t {I - 1}>(std::tuple<A &&...>(std::forward<A>(a)...));
        }

        template <class T>
        constexpr auto operator[](T && t) const {
            return std::bind(::asd::meta::detail::subscript(), *this, std::forward<T>(t));
        }
    };

    inline namespace placeholders
    {
        constexpr placeholder<1> _1 {};
        constexpr placeholder<2> _2 {};
        constexpr placeholder<3> _3 {};
        constexpr placeholder<4> _4 {};
        constexpr placeholder<5> _5 {};
        constexpr placeholder<6> _6 {};
        constexpr placeholder<7> _7 {};
        constexpr placeholder<8> _8 {};
        constexpr placeholder<9> _9 {};
    }

    template <int I>
    struct ::std::is_placeholder<asd::meta::placeholder<I>> :
        integral_constant<int, I> {};
}

#define prefix_op_function(op, name)                        \
    struct name                                             \
    {                                                       \
        template <class T>                                  \
        constexpr decltype(auto) operator()(T && t) const { \
            return op std::forward<T>(t);                   \
        }                                                   \
    };

#define postfix_op_function(op, name)                       \
    struct name                                             \
    {                                                       \
        template <class T>                                  \
        constexpr decltype(auto) operator()(T && t) const { \
            return std::forward<T>(t) op;                   \
        }                                                   \
    };

#define binary_op_function(op, name)                                    \
    struct name                                                         \
    {                                                                   \
        template <class T1, class T2>                                   \
        constexpr decltype(auto) operator()(T1 && t1, T2 && t2) const { \
            return std::forward<T1>(t1) op std::forward<T2>(t2);        \
        }                                                               \
    };

#define prefix_op_lambda(op, fn)                    \
    template <::asd::meta::detail::unary_lambda A>  \
    auto operator op(A && a) {                      \
        return std::bind(fn(), std::forward<A>(a)); \
    }

#define postfix_op_lambda(op, fn)                   \
    template <::asd::meta::detail::unary_lambda A>  \
    auto operator op(A && a, int) {                 \
        return std::bind(fn(), std::forward<A>(a)); \
    }

#define binary_op_lambda(op, fn)                                        \
    template <class A, class B>                                         \
        requires ::asd::meta::detail::binary_lambda<A, B>               \
    auto operator op(A && a, B && b) {                                  \
        return std::bind(fn(), std::forward<A>(a), std::forward<B>(b)); \
    }

namespace asd::meta
{
    namespace detail
    {
        template <class T, class T2 = plain<T>>
        concept lambda_expression = (std::is_placeholder_v<T2> != 0) || std::is_bind_expression_v<T2>;

        template <class A>
        concept unary_lambda = lambda_expression<A>;

        template <class A, class B>
        concept binary_lambda = lambda_expression<A> || lambda_expression<B>;

        prefix_op_function(+, unary_plus)
        prefix_op_function(*, dereference)

        prefix_op_function(++, increment)
        prefix_op_function(--, decrement)

        postfix_op_function(++, postfix_increment)
        postfix_op_function(--, postfix_decrement)

        binary_op_function(<<, left_shift)
        binary_op_function(>>, right_shift)

        binary_op_function(+=, plus_equal)
        binary_op_function(-=, minus_equal)
        binary_op_function(*=, multiplies_equal)
        binary_op_function(/=, divides_equal)
        binary_op_function(%=, modulus_equal)
        binary_op_function(&=, bit_and_equal)
        binary_op_function(|=, bit_or_equal)
        binary_op_function(^=, bit_xor_equal)
        binary_op_function(<<=, left_shift_equal)
        binary_op_function(>>=, right_shift_equal)
    }

    binary_op_lambda(+, std::plus<>)
    binary_op_lambda(-, std::minus<>)
    binary_op_lambda(*, std::multiplies<>)
    binary_op_lambda(/, std::divides<>)
    binary_op_lambda(%, std::modulus<>)
    prefix_op_lambda(-, std::negate<>)

    binary_op_lambda(==, std::equal_to<>)
    binary_op_lambda(!=, std::not_equal_to<>)
    binary_op_lambda(>, std::greater<>)
    binary_op_lambda(<, std::less<>)
    binary_op_lambda(>=, std::greater_equal<>)
    binary_op_lambda(<=, std::less_equal<>)

    binary_op_lambda(&&, std::logical_and<>)
    binary_op_lambda(||, std::logical_or<>)
    prefix_op_lambda(!, std::logical_not<>)

    binary_op_lambda(&, std::bit_and<>)
    binary_op_lambda(|, std::bit_or<>)
    binary_op_lambda(^, std::bit_xor<>)
    prefix_op_lambda(~, std::bit_not<>)

    binary_op_lambda(<<, ::asd::meta::detail::left_shift)
    binary_op_lambda(>>, ::asd::meta::detail::right_shift)

    prefix_op_lambda(+, ::asd::meta::detail::unary_plus)
    prefix_op_lambda(*, ::asd::meta::detail::dereference)

    prefix_op_lambda(++, ::asd::meta::detail::increment)
    prefix_op_lambda(--, ::asd::meta::detail::decrement)

    postfix_op_lambda(++, ::asd::meta::detail::postfix_increment)
    postfix_op_lambda(--, ::asd::meta::detail::postfix_decrement)

    binary_op_lambda(+=, ::asd::meta::detail::plus_equal)
    binary_op_lambda(-=, ::asd::meta::detail::minus_equal)
    binary_op_lambda(*=, ::asd::meta::detail::multiplies_equal)
    binary_op_lambda(/=, ::asd::meta::detail::divides_equal)
    binary_op_lambda(%=, ::asd::meta::detail::modulus_equal)
    binary_op_lambda(&=, ::asd::meta::detail::bit_and_equal)
    binary_op_lambda(|=, ::asd::meta::detail::bit_or_equal)
    binary_op_lambda(^=, ::asd::meta::detail::bit_xor_equal)
    binary_op_lambda(<<=, ::asd::meta::detail::left_shift_equal)
    binary_op_lambda(>>=, ::asd::meta::detail::right_shift_equal)
}

#undef prefix_op_function
#undef postfix_op_function
#undef binary_op_function
#undef prefix_op_lambda
#undef postfix_op_lambda
#undef binary_op_lambda
