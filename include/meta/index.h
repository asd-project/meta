//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <type_traits>

//---------------------------------------------------------------------------

namespace asd::meta
{
    using std::size_t;

    template <size_t N>
    using index = std::integral_constant<size_t, N>;

    namespace detail
    {
        constexpr int to_int(char c) {
            int result = 0;

            if (c >= 'A' && c <= 'F') {
                result = static_cast<int>(c) - static_cast<int>('A') + 10;
            }
            else if (c >= 'a' && c <= 'f') {
                result = static_cast<int>(c) - static_cast<int>('a') + 10;
            }
            else {
                result = static_cast<int>(c) - static_cast<int>('0');
            }

            return result;
        }

        template<size_t N>
        constexpr size_t parse(const char (&arr)[N]) {
            size_t base = 10;
            size_t offset = 0;

            if constexpr (N > 2) {
                if (arr[0] == '0') {
                    if (arr[1] == 'x') {        // hex
                        base = 16;
                        offset = 2;
                    } else if (arr[1] == 'b') { // bin
                        base = 2;
                        offset = 2;
                    } else {                    // oct
                        base = 8;
                        offset = 1;
                    }
                }
            }

            size_t number = 0;
            size_t multiplier = 1;

            for (size_t i = 0; i < N - offset; ++i) {
                if (char c = arr[N - 1 - i]; c != '\'') { // skip digit separators
                    number += to_int(c) * multiplier;
                    multiplier *= base;
                }
            }

            return number;
        }
    }

    namespace literals
    {
        template <char ...Chars>
        constexpr auto operator"" _c() {
            return index<asd::meta::detail::parse<sizeof...(Chars)>({Chars...})>{};
        }
    }
}
