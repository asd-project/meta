//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "macro.h"

#include <cstdint>
#include <cstddef>

#include <type_traits>
#include <initializer_list>
#include <array>

#if !defined(_MSC_VER) && !defined(__clang__)
namespace std
{
    template <class T>
    using remove_extent_t = typename remove_extent<T>::type;

    template <class T>
    using make_signed_t = typename make_signed<T>::type;

    template <class T>
    using make_unsigned_t = typename make_unsigned<T>::type;

    template <bool value>
    using bool_constant = integral_constant<bool, value>;
}
#endif

namespace asd
{
    inline namespace common_types
    {
        template <class T>
        using plain = std::remove_cv_t<std::remove_reference_t<T>>;

    #define plaintype(...) ::asd::plain<decltype(__VA_ARGS__)>

    //---------------------------------------------------------------------------

        template <class T>
        struct identity
        {
            using type = T;
        };

        template <class T>
        using identity_t = typename identity<T>::type;

        template <class T>
        using non_deduced = typename identity<T>::type;

    //---------------------------------------------------------------------------

        template <class ... Ts>
        struct overloaded : Ts... { using Ts::operator()...; };

        template <class ... Ts>
        overloaded(Ts...) -> overloaded<Ts...>;

    //---------------------------------------------------------------------------

        using int8  = char;
        using int16 = short;
        using int32 = int;
        using int64 = long long;

        using uint8  = unsigned char;
        using uint16 = unsigned short;
        using uint32 = unsigned int;
        using uint64 = unsigned long long;

    #ifdef __GNUC__
        using errno_t = int;
    #endif


    #ifndef _SSIZE_T_
    #define _SSIZE_T_
        using ssize_t = typename std::make_signed<size_t>::type;
    #endif

    //---------------------------------------------------------------------------

        using i8 = int8;
        using u8 = uint8;
        using i16 = int16;
        using u16 = uint16;
        using i32 = int32;
        using u32 = uint32;
        using i64 = int64;
        using u64 = uint64;
        using f32 = float;
        using f64 = double;

        using u8v2  = std::array<u8, 2>;
        using u8v3  = std::array<u8, 3>;
        using u8v4  = std::array<u8, 4>;

        using i32v2 = std::array<i32, 2>;
        using i32v3 = std::array<i32, 3>;
        using i32v4 = std::array<i32, 4>;
        using i32m2 = std::array<i32v2, 2>;
        using i32m3 = std::array<i32v3, 3>;
        using i32m4 = std::array<i32v4, 4>;

        using f32v2 = std::array<f32, 2>;
        using f32v3 = std::array<f32, 3>;
        using f32v4 = std::array<f32, 4>;
        using f32m2 = std::array<f32v2, 2>;
        using f32m3 = std::array<f32v3, 3>;
        using f32m4 = std::array<f32v4, 4>;

        using f64v2 = std::array<f64, 2>;
        using f64v3 = std::array<f64, 3>;
        using f64v4 = std::array<f64, 4>;
        using f64m2 = std::array<f64v2, 2>;
        using f64m3 = std::array<f64v3, 3>;
        using f64m4 = std::array<f64v4, 4>;
    }

    namespace meta
    {
        template <class T>
            requires(std::is_nothrow_move_assignable_v<T>)
        constexpr void swap(T & a, T & b) noexcept {
            auto tmp = std::move(a);
            a = std::move(b);
            b = std::move(tmp);
        }

        template <class A, class B>
        constexpr void swap(std::pair<A, B> & a, std::pair<A, B> & b) noexcept {
            meta::swap(a.first, b.first);
            meta::swap(a.second, b.second);
        }

        template <class ... T>
        constexpr bool always_false = false; // little utility to prevent immediate triggering of static_assert
    }
}
