//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "api.h"

#include <boost/throw_exception.hpp>

#if defined(__x86_64) || defined(_WIN64) || defined(__LP64__)
#define ARCH_X64
#else
#define ARCH_X86
#endif

#ifdef _MSC_VER
#define forceinline __forceinline
#else
#define forceinline __attribute__((always_inline))
#define __vectorcall
#define __thiscall
#endif

#define member_cast(member, ... /* type */)            \
    constexpr operator __VA_ARGS__ &() & {             \
        return this->member;                           \
    }                                                  \
                                                       \
    constexpr operator const __VA_ARGS__ &() const & { \
        return this->member;                           \
    }                                                  \
                                                       \
    constexpr operator __VA_ARGS__ &&() && {           \
        return std::move(this->member);                \
    }
