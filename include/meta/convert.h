//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "useif.h"
#include "types.h"

//---------------------------------------------------------------------------

namespace asd
{
    namespace detail
    {
        template <class To, class From, useif<std::is_same_v<asd::plain<From>, To>>>
        constexpr From && convert(meta::type<To>, From && from) {
            return std::forward<From>(from);
        }

        template <class To>
        struct convert_func
        {
            template <class From, class T = To>
            constexpr auto operator()(const From & v) const -> decltype(convert(meta::type_v<T>, v)) {
                return convert(meta::type_v<T>, v);
            }
        };
    }

    template <class To>
    constexpr auto convert = ::asd::detail::convert_func<To>{};

    template <class T, class U>
    concept tag_convertible = requires(T v) {
        requires not std::same_as<T, U>;
        { convert<U>(v) } -> std::convertible_to<U>;
    };
}
