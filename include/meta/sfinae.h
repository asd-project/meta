//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "types.h"
#include <boost/preprocessor/cat.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    template <class T>
    struct sfinae_int
    {
        sfinae_int(int) {}
    };

    //---------------------------------------------------------------------------

#define type_checker(name, type)                                                   \
    template <class T>                                                             \
    struct name                                                                    \
    {                                                                              \
    private:                                                                       \
        template <typename U>                                                      \
        static std::true_type _(::asd::sfinae_int<typename U::type>);              \
        template <typename U>                                                      \
        static std::false_type _(...);                                             \
                                                                                   \
    public:                                                                        \
        static constexpr bool value = ::asd::identity_t<decltype(_<T>(0))>::value; \
    };                                                                             \
                                                                                   \
    template <class T>                                                             \
    constexpr bool BOOST_PP_CAT(name, _v) = name<T>::value;

#define function_checker(name, func)                                                        \
    template <class... A>                                                                   \
    struct name                                                                             \
    {                                                                                       \
    private:                                                                                \
        template <typename... B>                                                            \
        static std::true_type _(::asd::sfinae_int<decltype(func(::std::declval<B>()...))>); \
        template <typename... B>                                                            \
        static std::false_type _(...);                                                      \
                                                                                            \
    public:                                                                                 \
        static constexpr bool value = ::asd::identity_t<decltype(_<A...>(0))>::value;       \
    };                                                                                      \
                                                                                            \
    template <class... A>                                                                   \
    constexpr bool BOOST_PP_CAT(name, _v) = name<A...>::value;

#define method_checker(name, method)                                                                          \
    template <class T, class... A>                                                                            \
    struct name                                                                                               \
    {                                                                                                         \
    private:                                                                                                  \
        template <typename U>                                                                                 \
        static std::true_type _(::asd::sfinae_int<decltype(std::declval<U>().method(std::declval<A>()...))>); \
        template <typename U>                                                                                 \
        static std::false_type _(...);                                                                        \
                                                                                                              \
    public:                                                                                                   \
        static constexpr bool value = ::asd::identity_t<decltype(_<T>(0))>::value;                            \
    };                                                                                                        \
                                                                                                              \
    template <class T, class... A>                                                                            \
    constexpr bool BOOST_PP_CAT(name, _v) = name<T, A...>::value;

#define static_method_checker(name, method)                                                    \
    template <class T, class... A>                                                             \
    struct name                                                                                \
    {                                                                                          \
    private:                                                                                   \
        template <typename U>                                                                  \
        static std::true_type _(::asd::sfinae_int<decltype(U::method(std::declval<A>()...))>); \
        template <typename U>                                                                  \
        static std::false_type _(...);                                                         \
                                                                                               \
    public:                                                                                    \
        static constexpr bool value = ::asd::identity_t<decltype(_<T>(0))>::value;             \
    };                                                                                         \
                                                                                               \
    template <class T, class... A>                                                             \
    constexpr bool BOOST_PP_CAT(name, _v) = name<T, A...>::value;

#define member_checker(name, member)                                               \
    template <class T>                                                             \
    struct name                                                                    \
    {                                                                              \
    private:                                                                       \
        template <typename U>                                                      \
        static std::true_type _(::asd::sfinae_int<decltype(U::member)>);           \
        template <typename U>                                                      \
        static std::false_type _(...);                                             \
                                                                                   \
    public:                                                                        \
        static constexpr bool value = ::asd::identity_t<decltype(_<T>(0))>::value; \
    };                                                                             \
                                                                                   \
    template <class T>                                                             \
    constexpr bool BOOST_PP_CAT(name, _v) = name<T>::value;

#define operator_checker(name, op)                                                                    \
    template <class T>                                                                                \
    struct name                                                                                       \
    {                                                                                                 \
    private:                                                                                          \
        template <typename U>                                                                         \
        static std::true_type _(::asd::sfinae_int<decltype(std::declval<U>() op std::declval<U>())>); \
        template <typename U>                                                                         \
        static std::false_type _(...);                                                                \
                                                                                                      \
    public:                                                                                           \
        static constexpr bool value = ::asd::identity_t<decltype(_<T>(0))>::value;                    \
    };                                                                                                \
                                                                                                      \
    template <class T>                                                                                \
    constexpr bool BOOST_PP_CAT(name, _v) = name<T>::value;

    operator_checker(is_less_comparable, <);
    operator_checker(is_equal_comparable, ==);

    //---------------------------------------------------------------------------

    method_checker(is_pointerable, operator->);

    //---------------------------------------------------------------------------

#define type_getter(name, T, tgt, def)              \
    template <class T>                              \
    struct name                                     \
    {                                               \
    private:                                        \
        template <class U>                          \
        static auto _(int) -> typename U::tgt;      \
        template <class U>                          \
        static auto _(...) -> def;                  \
                                                    \
    public:                                         \
        using type = identity_t<decltype(_<T>(0))>; \
    }
}
