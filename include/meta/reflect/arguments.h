//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/types.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class F>
    struct arguments;

    template <class R, class ... A>
    struct arguments<R (*)(A && ...)>
    {
        using type = meta::types<A &&...>;
    };

    template <class F>
    struct method_arguments;

    template <class C, class R, class ... A>
    struct method_arguments<R (C::*)(A...)>
    {
        using type = meta::types<A...>;
    };

    template <class C, class R, class ... A>
    struct method_arguments<R (C::*)(A...) const>
    {
        using type = meta::types<A...>;
    };

    template <class F>
    constexpr typename arguments<F>::type arguments_v = {};

    template <auto Method>
    constexpr typename method_arguments<decltype(Method)>::type method_arguments_v = {};
}
