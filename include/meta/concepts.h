//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <concepts>

#include "types.h"

//---------------------------------------------------------------------------

namespace asd
{
    template <class T, class U>
    concept same_as = std::is_same_v<U, plain<T>>;

    template <class T, class U>
    concept not_same_as = !std::is_same_v<U, plain<T>>;

    template <class T, class U>
    concept derived_from = std::is_base_of_v<U, plain<T>>;

    template <class T, class U>
    concept not_derived_from = !std::is_base_of_v<U, plain<T>>;

    template <class T, class U>
    concept convertible_to = std::is_convertible_v<T, U>;

    template <class T, class U>
    concept not_convertible_to = !std::is_convertible_v<T, U>;

    template <class T>
    concept enum_type = std::is_enum_v<T>;
    
    template <class T>
    concept awaitable = requires (T v) {
        { v.await_ready() } -> convertible_to<bool>;
        { v.await_resume() };
    };

    template <class T, class R>
    concept awaitable_to = requires (T v) {
        { v.await_ready() } -> convertible_to<bool>;
        { v.await_resume() } -> convertible_to<R>;
    };
}
