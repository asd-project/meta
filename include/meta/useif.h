//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include "concepts.h"
#include <boost/preprocessor/cat.hpp>

//---------------------------------------------------------------------------

    /**
     *  useif, skipif, selectif
     *
      *  @brief
     *      Used to perform SFINAE operations by the template argument.
     *      ...    - parameters of the operation
     *
     *  Examples:
     *      template <class T, int N_, useif<N != N_>>
     *      Class & operator = (const T (&val)[N_])
     *      {
     *          ...
     *          return *this;
     *      }
     *
     *      template <typename T, selectif(0)<same_type<T, std::string>::value>>
     *      Class & operator = (const T & val)
     *      {
     *          ...
     *          return *this;
     *      }
     *
     *      template <typename T, selectif(1)<same_type<T, std::wstring>::value>>
     *      Class & operator = (const T & val)
     *      {
     *          ...
     *          return *this;
     *      }
     *  or
     *      template <int N_, skipif<N == N_>>
     *      Class<T, N> & operator = (const T (&val)[N_]);
     *
     *      template <int N_, skipped_t>
     *      Class<T, N> & operator = (const T (&val)[N_])
     *      {
     *          ...
     *          return *this;
     *      }
     */

template <class T, bool ... Test>
struct use_filter_t {};


// S - the sfinae class, t - the enum type, which identifies filter, f - the filter, B... - the sequence of filter expressions
#define _useif_class(S)     struct S { enum t { v }; template <bool ... B> using f = typename use_filter_t<S, B...>::type; }

// "S::t = S::t::v" - removes ambiguity by the different S::t enums, "class = S::f" - applies sfinae filter
#define _useif(S)           typename S::t = S::v, class = typename S::f

#define _useif_t(S)         typename S::t, class

_useif_class(sfinae_use);
_useif_class(sfinae_skip);

template <class Case>
_useif_class(sfinae_select);

//---------------------------------------------------------------------------

#define useif               class = std::enable_if_t
#define skipif              _useif(::sfinae_skip)
#define selectif(Case)      _useif(::sfinae_select<struct BOOST_PP_CAT(case_, Case)>)

#define used_t              class
#define skipped_t           _useif_t(::sfinae_skip)
#define selected_t(Case)    _useif_t(::sfinae_select<struct BOOST_PP_CAT(case_, Case)>)

//---------------------------------------------------------------------------

template <>
struct use_filter_t<sfinae_use> : asd::identity<typename sfinae_use::t> {};

template <bool ... Others>
struct use_filter_t<sfinae_use, true, Others...> : use_filter_t<sfinae_use, Others...> {};

template <bool ... Others>
struct use_filter_t<sfinae_use, false, Others...> {};

template <class Case>
struct use_filter_t<sfinae_select<Case>> : asd::identity<typename sfinae_select<Case>::t> {};

template <class Case, bool ... Others>
struct use_filter_t<sfinae_select<Case>, true, Others...> : use_filter_t<sfinae_select<Case>, Others...> {};

template <class Case, bool ... Others>
struct use_filter_t<sfinae_select<Case>, false, Others...> {};

template <bool ... Others>
struct use_filter_t<sfinae_skip, false, Others...> : asd::identity<typename sfinae_skip::t> {};

template <bool ... Others>
struct use_filter_t<sfinae_skip, true, Others...> : use_filter_t<sfinae_skip, Others...> {};

template <>
struct use_filter_t<sfinae_skip> {};
