
#pragma once

//---------------------------------------------------------------------------

#ifdef _MSC_VER
    #ifdef USE_SHARED_LIBS
        #define __META_API_PASS(...) __VA_ARGS__

        #define __META_API_CAT(a, ...) a##__VA_ARGS__

        #define __META_API_IF_ELSE_IMPL_0(t, f) f
        #define __META_API_IF_ELSE_IMPL_1(t, f) t
        #define __META_API_IF_ELSE_IMPL(c)      __META_API_CAT(__META_API_IF_ELSE_IMPL_, c)

        #define __META_API_SECOND_IMPL(a, b, ...) b
        #define __META_API_SECOND(...) __META_API_PASS(__META_API_SECOND_IMPL(__VA_ARGS__))
        #define __META_API_IF_ELSE(c) __META_API_IF_ELSE_IMPL(__META_API_PASS(__META_API_IS(c)))

        #define __META_API_CHECK(...) __META_API_SECOND(__VA_ARGS__, 0, ~)

        #define __META_API_IS_1()   ~, 1
        #define __META_API_IS_()    ~, 1
        #define __META_API_IS(...)  __META_API_CHECK(__META_API_CAT(__META_API_IS_, __VA_ARGS__)())

        #define asd_external_api 1

        #define export_api(module)                                                      \
            __META_API_IF_ELSE(__META_API_CAT(asd_, __META_API_CAT(module, _api)))(     \
                __declspec(dllexport),                                                  \
                __declspec(dllimport)                                                   \
            )
    #else
        #define export_api(module)
    #endif
#elif defined(__GNUC__) || defined(__clang__)
    #define export_api(module) __attribute__((visibility("default")))
#else
    #define export_api(module)
#endif
