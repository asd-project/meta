//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <type_traits>
#include <compare>
#include <functional>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <class T, class Tag, class InvalidId = void>
    struct id
    {
        using underlying_type = T;

        constexpr id(T value) noexcept : value(value) {}

        constexpr id(const id &) noexcept = default;
        constexpr id & operator = (const id &) noexcept = default;

        template <class U, class UTag>
            requires(!std::is_same_v<T, U> || !std::is_same_v<Tag, UTag>)
        constexpr id(const id<U, UTag> &) noexcept = delete;

        template <class U, class UTag>
            requires(!std::is_same_v<T, U> || !std::is_same_v<Tag, UTag>)
        constexpr id & operator = (const id<U, UTag> &) noexcept = delete;

        constexpr operator T () const noexcept {
            return this->value;
        }

        constexpr id & operator ++ () noexcept {
            ++this->value;
            return *this;
        }

        constexpr id operator ++ (int) noexcept {
            return this->value++;
        }

        constexpr id & operator -- () noexcept {
            --this->value;
            return *this;
        }

        constexpr id operator -- (int) noexcept {
            return this->value--;
        }

        constexpr id & operator += (T delta) noexcept {
            this->value += value;
            return *this;
        }

        constexpr id & operator -= (T value) noexcept {
            this->value -= value;
            return *this;
        }

        constexpr T * operator & () noexcept {
            return &this->value;
        }

        constexpr const T * operator & () const noexcept {
            return &this->value;
        }

        friend constexpr std::strong_ordering operator <=>(const id & a, const id & b) {
            return static_cast<T>(a) <=> static_cast<T>(b);
        }

        T value;
    };

    template <class T, class U, class Tag, class InvalidId>
        requires(!std::same_as<id<T, Tag, InvalidId>, plain<U>> && requires(T a, U b) {
            { a <=> b };
        })
    constexpr std::strong_ordering operator <=>(const id<T, Tag, InvalidId> & a, const U & b) {
        return static_cast<T>(a) <=> b;
    }

    template <class T, class Tag, T InvalidId>
    struct id<T, Tag, std::integral_constant<T, InvalidId>> : id<T, Tag>
    {
        using id<T, Tag>::id;

        constexpr id() noexcept : id<T, Tag>(InvalidId) {}
        constexpr id(const id & i) noexcept : id<T, Tag>(i.value) {}
        constexpr id(id && i) noexcept : id<T, Tag>(std::exchange(i.value, InvalidId)) {}

        template <class U, class UTag, class UInvalidId>
            requires (not std::same_as<id, id<U, UTag, UInvalidId>>)
        constexpr explicit id(const id<U, UTag, UInvalidId> & i) noexcept :
            id<T, Tag>(static_cast<T>(i.value)) {}

        constexpr id & operator = (const id & i) noexcept {
            this->value = i.value;
            return *this;
        }

        constexpr id & operator = (id && i) noexcept {
            std::swap(this->value, i.value);
            return *this;
        }

        bool valid() const noexcept {
            return this->value != InvalidId;
        }
    };
}

namespace std
{
    template<class T, class Tag, class InvalidId>
    struct hash<asd::meta::id<T, Tag, InvalidId>> : hash<T> {};
}
