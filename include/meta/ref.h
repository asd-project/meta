//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <functional>

//---------------------------------------------------------------------------

namespace asd
{
    template <class T>
    using ref = std::reference_wrapper<T>;

    namespace make
    {
        template <class T>
        constexpr decltype(auto) ref(T & v) {
            return std::ref(v);
        }

        template <class T>
        constexpr decltype(auto) cref(T & v) {
            return std::cref(v);
        }

        template <class T>
        constexpr decltype(auto) ref(std::reference_wrapper<T> v) {
            return std::ref(v);
        }

        template <class T>
        constexpr decltype(auto) cref(std::reference_wrapper<T> v) {
            return std::cref(v);
        }
    }
}
