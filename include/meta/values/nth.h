//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/types.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <size_t I, typename T, T ... A>
        struct split_sequence {};

        template <size_t I, typename T, T H, T ... A>
        struct split_sequence<I, T, H, A...> : split_sequence<I - 1, T, A...> {};

        template <typename T, T H, T ... A>
        struct split_sequence<0, T, H, A...>
        {
            static constexpr T head = H;
            using tail = std::integer_sequence<T, H, A...>;
        };

        template <size_t I, typename T>
        struct split_sequence<I, T>
        {
            static constexpr T head = {};
            using tail = std::integer_sequence<T>;
        };
    }

    template <size_t Index, typename T, T ... A>
    constexpr T nth_v = asd::meta::detail::split_sequence<Index, T, A...>::head;

    template <typename T, T ... A>
    constexpr T first_v = nth_v<0, T, A...>;

    template <typename T, T ... A>
    constexpr T last_v = nth_v<sizeof...(A) - 1, T, A...>;
}
