//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <type_traits>
#include <boost/integer.hpp>
#include <meta/concepts.h>
#include <magic_enum.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    template <class F, class S>
    constexpr auto check_flag(F flag, S set) -> decltype((flag & set) == flag) {
        return static_cast<F>(flag & set) == flag;
    }

    template <class F, class S, typename = decltype(std::declval<S>() | std::declval<F>())>
    constexpr auto set_flag(F flag, S & set) -> S & {
        return set = static_cast<S>(set | flag);
    }

    template <class F, class S, typename = decltype(std::declval<S>() & ~std::declval<F>())>
    constexpr auto clear_flag(F flag, S & set) -> S & {
        return set = static_cast<S>(set & ~flag);
    }

    template <class F, class S, typename = decltype(std::declval<S>() | std::declval<F>()), typename = decltype(std::declval<S>() & ~std::declval<F>())>
    constexpr auto toggle_flag(F flag, S & set, bool state) -> S & {
        return set = (state ? static_cast<S>(set | flag) : static_cast<S>(set & ~flag));
    }

    template <class F, class S, typename = decltype(std::declval<S>() | std::declval<F>()), typename = decltype(std::declval<S>() & ~std::declval<F>())>
    constexpr auto toggle_flag(F flag, S & set) -> S & {
        return set = static_cast<S>(set ^ flag);
    }

    template <class F, class S>
    constexpr auto has_some_flags(F flags, S set) -> decltype((flags & set) != 0) {
        return (flags & set) != 0;
    }

    template <class E>
    struct optimal_flag_type
    {
        using type = std::conditional_t<std::is_enum_v<E>, typename boost::uint_value_t<(1 << (magic_enum::enum_count<E>() - 1))>::least, E>;
    };

    template <class E>
    using optimal_flag_type_t = typename optimal_flag_type<E>::type;

    template <class E>
    struct enum_flags
    {
        static_assert(asd::enum_type<E>);

        using value_type = optimal_flag_type_t<E>;

        constexpr enum_flags() noexcept = default;
        constexpr enum_flags(const enum_flags & f) noexcept = default;
        constexpr enum_flags(const value_type & value) noexcept : value(value) {}
        constexpr enum_flags(E v) noexcept : value(static_cast<value_type>(1 << static_cast<value_type>(v))) {}

        template <class ... T> requires ((sizeof ... (T) > 1) && (std::is_same_v<T, E> && ...))
        constexpr enum_flags(T ... v) noexcept : value((... | static_cast<value_type>(1 << static_cast<value_type>(v)))) {}

        constexpr enum_flags & operator = (const enum_flags<E> & f) noexcept = default;

        constexpr enum_flags & operator = (E v) noexcept {
            value = static_cast<value_type>(1 << static_cast<value_type>(v));
            return * this;
        }

        constexpr bool check(E v) const noexcept {
            return check_flag(1 << static_cast<value_type>(v), value);
        }

        constexpr bool check(enum_flags<E> f) const noexcept {
            return check_flag(f.value, value);
        }

        constexpr bool any() const noexcept {
            return value != 0;
        }

        constexpr bool any(enum_flags<E> f) const noexcept {
            return has_some_flags(f.value, value);
        }

        constexpr bool none() const noexcept {
            return value == 0;
        }

        constexpr bool none(enum_flags<E> f) const noexcept {
            return !has_some_flags(f.value, value);
        }

        constexpr auto set(E v) noexcept {
            return set_flag(1 << static_cast<value_type>(v), value);
        }

        constexpr auto set(enum_flags<E> f) noexcept {
            return set_flag(f, value);
        }

        constexpr auto clear(E v) noexcept {
            return clear_flag(1 << static_cast<value_type>(v), value);
        }

        constexpr auto clear(enum_flags<E> f) noexcept {
            return clear_flag(f, value);
        }

        constexpr auto toggle(E v) noexcept {
            return toggle_flag(1 << static_cast<value_type>(v), value);
        }

        constexpr auto toggle(enum_flags<E> f) noexcept {
            return toggle_flag(f, value);
        }

        constexpr auto toggle(E v, bool state) noexcept {
            return toggle_flag(1 << static_cast<value_type>(v), value, state);
        }

        constexpr auto toggle(enum_flags<E> f, bool state) noexcept {
            return toggle_flag(f, value, state);
        }

        constexpr operator value_type () const noexcept {
            return value;
        }

        constexpr enum_flags operator ~ () const noexcept {
            return ~value;
        }

        friend constexpr enum_flags operator | (enum_flags a, enum_flags b) noexcept {
            return a.value | b.value;
        }

        friend constexpr enum_flags operator & (enum_flags a, enum_flags b) noexcept {
            return a.value & b.value;
        }

        friend constexpr enum_flags operator == (enum_flags a, enum_flags b) noexcept {
            return a.value == b.value;
        }

    private:
        value_type value {0};
    };

#define adapt_enum_flags(Enum)                                                      \
    constexpr asd::enum_flags<Enum> operator | (Enum a, Enum b) {                   \
        return asd::enum_flags<Enum>(a) | asd::enum_flags<Enum>(b);                 \
    }                                                                               \
                                                                                    \
    constexpr asd::enum_flags<Enum> operator | (asd::enum_flags<Enum> a, Enum b) {  \
        return a | asd::enum_flags<Enum>(b);                                        \
    }                                                                               \
                                                                                    \
    constexpr asd::enum_flags<Enum> operator | (Enum a, asd::enum_flags<Enum> b) {  \
        return asd::enum_flags<Enum>(a) | b;                                        \
    }                                                                               \
                                                                                    \
    constexpr asd::enum_flags<Enum> operator ~ (Enum value) {                       \
        return ~asd::enum_flags<Enum>(value);                                       \
    }

}

namespace std
{
    template<class E>
    struct hash<asd::enum_flags<E>> : hash<typename asd::enum_flags<E>::value_type> {};
}
