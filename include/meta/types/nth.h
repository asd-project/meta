//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <size_t I, class Types>
    using nth_t = std::tuple_element_t<I, plain<Types>>;

    template <class Types>
    using first_t = nth_t<0, Types>;

    template <class Types>
    using last_t = nth_t<std::tuple_size_v<Types> - 1, Types>;
}
