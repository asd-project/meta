//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template<class ... T>
        struct concat {};

        template<class ... Types>
        struct concat<types<Types...>>
        {
            using type = types<Types...>;
        };

        template<>
        struct concat<>
        {
            using type = types<>;
        };

        template<class H, class ... Tail>
        struct concat<H, Tail...> : concat<types<H>, Tail...> {};

        template<class ... Types, class H, class ... Tail>
        struct concat<types<Types...>, H, Tail...> : concat<types<Types..., H>, Tail...> {};

        template<class ... A, class ... B, class ... Tail>
        struct concat<types<A...>, types<B...>, Tail...> : concat<types<A..., B...>, Tail...> {};
    }

    template<class ... T>
    using concat_t = typename meta::detail::concat<T...>::type;
}
