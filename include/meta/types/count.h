//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <class T, class Types>
        struct count {};

        template <class T, class H, class ... Types>
        struct count<T, types<H, Types...>> : count<T, types<Types...>> {};

        template <class T, class ... Types>
        struct count<T, types<T, Types ...>> : std::integral_constant<int, 1 + count<T, types<Types...>>::value> {};

        template <class T>
        struct count<T, types<>> : std::integral_constant<int, 0> {};

        // tuple compat
        template <class T, class ... Types>
        struct count<T, std::tuple<Types ...>> : count<T, types<Types...>> {};
    }

    template <class T, class Types>
    constexpr int count_v = meta::detail::count<T, Types>::value;
}
