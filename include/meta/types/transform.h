//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <class Types, template <class> class Transform>
        struct transform {};

        template <class ... T, template <class> class Transform>
        struct transform<types<T...>, Transform>
        {
            using type = types<typename Transform<plain<T>>::type...>;
        };

        template <class ... T, template <class> class Transform>
        struct transform<std::tuple<T...>, Transform>
        {
            using type = std::tuple<typename Transform<plain<T>>::type...>;
        };
    }

    template <class Types, template <class> class Transform>
    using transform_t = typename meta::detail::transform<Types, Transform>::type;
}
