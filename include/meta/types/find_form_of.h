//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <class A, class B>
        using is_form_of = std::conjunction<
            std::is_convertible<A, B>,
            std::is_same<plain<A>, plain<B>>
        >;

        template <int I, class Types, class T>
        struct find_form_of_type {};

        template <int I, class H, class ... Types, class T>
        struct find_form_of_type<I, types<H, Types...>, T> : std::conditional_t<
            is_form_of<H, T>::value,
            std::integral_constant<int, I>,
            find_form_of_type<I + 1, types<Types...>, T>
        > {};

        template <int I, class T>
        struct find_form_of_type<I, types<>, T> : std::integral_constant<int, -1> {};

        // tuple compat
        template <int I, class ... Types, class T>
        struct find_form_of_type<I, std::tuple<Types ...>, T> : find_form_of_type<I, types<Types...>, T> {};
    }

    template <class Types, class T>
    constexpr int find_form_of_v = meta::detail::find_form_of_type<0, Types, T>::value;
}
