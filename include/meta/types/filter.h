//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/types/concat.h>
#include <meta/types/nth.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <class Types, template <class> class Predicate>
        struct filter {};

        template <class H, class ... T, template <class> class Predicate>
        struct filter<types<H, T...>, Predicate>
        {
            using tail = typename filter<types<T...>, Predicate>::type;
            using type = std::conditional_t<Predicate<H>::value, meta::concat_t<H, tail>, tail>;
        };

        template <template <class> class Predicate>
        struct filter<types<>, Predicate>
        {
            using type = types<>;
        };

        // tuple compat
        template <class ... T, template <class> class Predicate>
        struct filter<std::tuple<T...>, Predicate>
        {
            using type = typename filter<types<T...>, Predicate>::type::tuple_type;
        };
    }

    template <class Types, template <class> class Predicate>
    using filter_t = typename meta::detail::filter<Types, Predicate>::type;

    namespace detail
    {
        template <size_t Index, template <class> class Predicate, size_t ... Indices, class Tuple>
        constexpr auto make_filter_sequence(const Tuple & t) {
            if constexpr(Index >= std::tuple_size_v<Tuple>) {
                return std::index_sequence<Indices...>{};
            } else if constexpr(Predicate<nth_t<Index, Tuple>>::value) {
                return make_filter_sequence<Index + 1, Predicate, Indices..., Index>(t);
            } else {
                return make_filter_sequence<Index + 1, Predicate, Indices...>(t);
            }
        }
    }

    template <template <class> class Predicate, class Tuple>
    constexpr auto make_filter_sequence(const Tuple & t) {
        return asd::meta::detail::make_filter_sequence<0, Predicate>(t);
    }
}
