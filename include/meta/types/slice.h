//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/slice.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    template <size_t Begin, size_t End, class Tuple>
    using slice_t = decltype(slice<Begin, End>(std::declval<Tuple>()));
}
