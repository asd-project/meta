//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>
#include <meta/types/concat.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <class Types, template <class> class Predicate>
        struct remove_if {};

        template <class H, class ... T, template <class> class Predicate>
        struct remove_if<types<H, T...>, Predicate>
        {
            using tail = typename remove_if<types<T...>, Predicate>::type;
            using type = std::conditional_t<!Predicate<H>::value, meta::concat_t<H, tail>, tail>;
        };

        template <template <class> class Predicate>
        struct remove_if<types<>, Predicate>
        {
            using type = types<>;
        };

        // tuple compat
        template <class ... T, template <class> class Predicate>
        struct remove_if<std::tuple<T...>, Predicate>
        {
            using type = typename remove_if<types<T...>, Predicate>::type::tuple_type;
        };
    }

    template <class Types, template <class> class Predicate>
    using remove_if_t = typename meta::detail::remove_if<Types, Predicate>::type;

    namespace detail
    {
        template <size_t Index, template <class> class Predicate, size_t ... Indices, class Tuple>
        constexpr auto make_remove_if_sequence(const Tuple & t) {
            if constexpr(Index >= std::tuple_size_v<Tuple>) {
                return std::index_sequence<Indices...>{};
            } else if constexpr(!Predicate<nth_t<Index, Tuple>>::value) {
                return make_remove_if_sequence<Index + 1, Predicate, Indices..., Index>(t);
            } else {
                return make_remove_if_sequence<Index + 1, Predicate, Indices...>(t);
            }
        }
    }

    template <template <class> class Predicate, class Tuple>
    constexpr auto make_remove_if_sequence(const Tuple & t) {
        return asd::meta::detail::make_remove_if_sequence<0, Predicate>(t);
    }
}
