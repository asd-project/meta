//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple.h>

//---------------------------------------------------------------------------

namespace asd::meta
{
    namespace detail
    {
        template <int I, class Types, class T>
        struct find_type {};

        template <int I, class H, class ... Types, class T>
        struct find_type<I, types<H, Types...>, T> : find_type<I + 1, types<Types...>, T> {};

        template <int I, class ... Types, class T>
        struct find_type<I, types<T, Types ...>, T> : std::integral_constant<int, I> {};

        template <int I, class T>
        struct find_type<I, types<>, T> : std::integral_constant<int, -1> {};

        // tuple compat
        template <int I, class ... Types, class T>
        struct find_type<I, std::tuple<Types ...>, T> : find_type<I, types<Types...>, T> {};
    }

    template <class Types, class T>
    constexpr int find_v = meta::detail::find_type<0, Types, T>::value;
}
